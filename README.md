# Hugo Orbit Theme
Forked from [hugo-orbit-theme](https://github.com/aerohub/hugo-orbit-theme)

## Credits
Theme designer: [Xiaoying Riley](https://xiaoyingriley.com/)

Ported by: [Pavel Kanyshev](https://github.com/aerohub)

## Pull changes from upstream (dead)
Upstream not maintained anymore, we are on our own :-)
